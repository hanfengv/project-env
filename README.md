# Porject Environment

## overview

1. Lock Node.js Version
2. Lock PackageManager Version (npm/yarn/pnpm)

## useage

1. install [n](https://www.npmjs.com/package/n)
2. `npx project-env node=20.11.1 pnpm=8.15.4`

## advanced

```json
"scripts": {
  "project-env": "npx -y project-env node=18.19.1 pnpm=8.15.4",
  "preinstall": "npx -y project-env onlyAllow",
  "prestart": "npx -y project-env dev",
  "dev": "npm run start",
}
```

## project-env dev
1. `"scripts": { "dev": "npx project-env dev && echo hello-dev" }`
2. run `npm run dev` auto switch node versions

## default Options

1. `node=20.11.1`
2. `pnpm=8.15.4`
3. `npmMirror=https://registry.npmmirror.com/`
4. `nodeMirror=https://npmmirror.com/mirrors/node`
5. `debug=false`
6. `engines=true` 设置为false则不切换node版本

## features

```js
  // Expected environment 
  const wanted = getWanted()
  // Current environment
  const current = whichPmRuns()

  // Config .gitignore
  const lockFileSuccess = await gitignoreLockfile(wanted.name)

  // Config .npmrc
  const npmrcSuccess = await npmrcSettings()

  // Config package.json
  const packagejsonSuccess = await packageJson()

  // Check Node.js version
  const nodeSuccess = await checkNode(wanted, current)

  // Check PackageManager version
  const pmSuccess = await checkNpm(wanted)
```