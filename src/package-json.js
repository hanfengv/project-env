import { readFile, writeFile } from 'node:fs/promises'
import { PackageManagers, getConfigs, message } from './utils.js'

/**
 * 处理 package.json 文件
 * "engines" : { "node": "20.11.1" }
 * packageManager": "pnpm@8.7.6",
 */
export async function packageJson(node, manager, managerVersion) {
  let success = true
  const configs = getConfigs()
  try {
    const packagePath = './package.json'
    const jsonStr = await readFile(packagePath, { encoding: 'utf8', flag: 'a+' })
    const jsonData = JSON.parse(jsonStr)
    let isWrite = false

    // engines 字段
    const enginesDiff =
      !jsonData.engines || jsonData.engines.node !== node || jsonData.engines[manager] !== managerVersion
    if (configs.engines === 'true' && enginesDiff) {
      isWrite = true
      const engines = jsonData.engines || {}
      Object.keys(PackageManagers).forEach((item) => delete engines[item])
      jsonData.engines = {
        ...engines,
        node: node,
        [manager]: managerVersion,
      }
    }

    // packageManager 字段
    const packageManagerStr = `${manager}@${managerVersion}`
    if (jsonData.packageManager !== packageManagerStr) {
      isWrite = true
      jsonData.packageManager = packageManagerStr
    }

    // preinstall 字段

    if (isWrite) {
      const content = JSON.stringify(jsonData, null, 2)
      // message.log(" packageJson . content:", content);
      await writeFile(packagePath, content, { flag: 'w' })
    }
  } catch (err) {
    success = false
    message.error('packageJson . err:', err)
  }
  return success
}
