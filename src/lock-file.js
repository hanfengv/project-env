import { readFile, writeFile } from 'node:fs/promises'
import { PackageManagers, message } from './utils.js'

/**
 * 处理 .gitignore 文件
 * 保证包管理器lockfile一致性
 */
export async function gitignoreLockfile(packageManager = 'pnpm') {
  const PackageLocks = Object.values(PackageManagers)
  const PackageRegx = new RegExp(PackageLocks.join('|'))
  let isSuccess = true
  try {
    const gitignorePath = './.gitignore'
    const gitignoreCtx = await readFile(gitignorePath, { encoding: 'utf8', flag: 'a+' })
    const lines = gitignoreCtx.split('\n').filter((line) => !PackageRegx.test(line))
    Object.keys(PackageManagers).forEach((key) => {
      if (key !== packageManager) lines.push(PackageManagers[key])
    })

    const content = lines.join('\n')
    writeFile(gitignorePath, content, { flag: 'w' })
  } catch (err) {
    isSuccess = false
    message.error('gitignoreLockfile', err)
  }
  return isSuccess
}
