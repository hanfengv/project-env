import { exec } from 'node:child_process'
import { promisify } from 'node:util'

export const Debug = process.argv.includes('debug=true')

export const PackageManagers = {
  npm: 'package-lock.json',
  yarn: 'yarn.lock',
  pnpm: 'pnpm-lock.json',
  bun: 'bun.lockb',
}

export const message = {
  prefix: '🚀',
  suffix: '\n',
  success(msg, ...args) {
    console.info(`${this.prefix}\u001b[32m ${msg}`, ...args, this.suffix)
  },
  error(msg, ...args) {
    console.info(`${this.prefix}\u001b[31m ${msg}`, ...args, this.suffix)
  },
  log(...args) {
    Debug && console.log(`${this.prefix}\u001b[34m`, ...args, this.suffix)
  },
}

export const execAsync = promisify(exec)

/**
 * 从 process.argv 中获取参数
 */
export function getConfigs() {
  const args = process.argv.slice(2)
  const configs = {
    /** node版本 */
    node: '20.11.1',
    /** 包管理器及其版本 */
    pnpm: '8.15.4',
    /** npm源 */
    npmMirror: 'https://registry.npmmirror.com/',
    /** node源 */
    nodeMirror: 'https://npmmirror.com/mirrors/node',
    /** debug信息 */
    debug: 'false',
    /** 是否修改 `package.json` 的 `engines` 字段 */
    engines: 'true',
  }
  args.forEach((item) => {
    const [key, value] = item.split('=')
    configs[key] = value
  })

  if (configs.npm || configs.yarn || configs.bun) {
    delete configs.pnpm
  }

  return configs
}
/**
 * 期望的node版本 包管理器名称和版本 从 npm srcipt 命令中获取参数
 * @param {{node,pnpm,npmMirror,nodeMirror,debug}} configs
 * @returns {{ nodeVersion:string, name: string, version: string }}
 */
export function getWanted(configs) {
  const PMs = Object.keys(PackageManagers)
  const [name] = Object.keys(configs).filter((item) => PMs.includes(item))

  return {
    nodeVersion: configs.node,
    name,
    version: configs[name],
  }
}

/**
 * which-pm-runs
 * 通过 npm_config_user_agent 环境变量获取 包管理器名称 和 全局包管理器版本
 * https://github.com/zkochan/packages/blob/main/which-pm-runs/index.js
 */
export function whichPmRuns() {
  const userAgent = process.env.npm_config_user_agent
  console.log("🚀 . whichPmRuns . userAgent:", userAgent)
  if (!userAgent) {
    return undefined
  }
  const pmSpec = userAgent.split(' ')[0]
  const separatorPos = pmSpec.lastIndexOf('/')
  const name = pmSpec.substring(0, separatorPos)
  return {
    nodeVersion: process.version,
    name: name === 'npminstall' ? 'cnpm' : name,
    version: pmSpec.substring(separatorPos + 1),
  }
}

export function formatVersion(versionStr = '') {
  return versionStr.match(/[\d.]+/)[0] || ''
}

// 随机输出一个端口 https://blog.csdn.net/weixin_45418665/article/details/106100007
export async function randomPort() {
  const WellKnownPorts = [0, 21, 22, 23, 25, 80, 110, 143, 443, 1433, 1434, 1521, 3306, 3389, 8080, 3000]
  const portMax = 65535
  let whileFlag = 1
  let port
  // 最大尝试100次
  while (whileFlag) {
    port = parseInt(Math.random() * portMax)
    const command = `lsof -i :${port}`
    const { stdout } = await execAsync(command).catch((e) => ({ stdout: true }))
    if (!WellKnownPorts.includes(port) && (stdout === true || whileFlag > 100)) {
      whileFlag = false
      console.log("🚀 . randomPort . port:", port)
    } else {
      whileFlag++
    }
  }
  return port
}