import { execAsync, message, whichPmRuns } from './utils.js'

// https://nodejs.org/en/about/previous-releases
// const NodeVersions = { Iron: "20.11.1", Hydrogen: "18.19.1", Gallium: "16.20.2" };

/**
 * @param {string} wantedVersion
 * @param {string} currentVersion
 * @param {string} nodeMirror
 */
export async function checkNode(wantedVersion, currentVersion, nodeMirror) {
  if (currentVersion !== wantedVersion) {
    return await switchNode(wantedVersion, nodeMirror)
  } else {
    return true
  }
}

/**
 * 切换node版本
 * @param {string} nodeVersion
 * @param {string} [nodeMirror]
 * @returns
 */
export async function switchNode(nodeVersion, nodeMirror = '') {
  let success = true
  const command = nodeMirror ? `export N_NODE_MIRROR=${nodeMirror} & n auto` : 'n auto'
  try {
    const { stdout, stderr } = await execAsync(command)
    // message.log("🚀 . switchNode .  stdout, stderr:", { stdout, stderr });

    if (stdout.includes('installed')) {
      message.success('切换Node版本成功')
    } else {
      message.error(`此项目只能使用 node.js@${nodeVersion}; 请手动切换后版本后重试`, stderr)
      success = false
    }
  } catch (error) {
    message.log('切换Node版本异常,可能是由于设置了 engines=false')
  }

  return success
}
