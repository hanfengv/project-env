import { execAsync, message, whichPmRuns } from './utils.js'

/**
 * @param { {nodeVersion: string; name: string; version: string;}} wanted
 */
export async function checkNpm(wanted) {
  const current = whichPmRuns()

  // 这里有点小问题; current 的 version 返回的是全局包管理的version
  // 其实每次都 switchPm 也行
  if (!current || current.name !== wanted.name || current.version !== wanted.version) {
    return await switchPm(wanted.name, wanted.version) // 尝试切换包管理器版本
  } else {
    return true
  }
}

/**
 * 切换包管理
 * https://nodejs.org/dist/v20.9.0/docs/api/corepack.html
 * @param {*} name 包管理器名称
 * @param {*} version 包管理器版本
 */
async function switchPm(name, version) {
  // corepack enable && corepack prepare pnpm@8.9.2 --activate
  // TODO: 以后可能需要兼容一下新版本的 corepack use 语法
  const command = `corepack enable && corepack prepare ${name}@${version} --activate`
  message.log('checkNpm.command:', command)
  const { stdout, stderr } = await execAsync(command)
  let success = true
  if (!stderr) {
    message.success('切换包管理器成功')
  } else {
    message.error(`此项目只能使用 ${name}@${version} 安装依赖; 请手动切换后版本后重试`)
    // process.exit(1);
    success = false
  }
  return success
}
