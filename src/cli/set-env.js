import { getWanted, message, whichPmRuns, getConfigs } from '../utils.js'
import { gitignoreLockfile } from '../lock-file.js'
import { npmrcSettings } from '../npmrc-file.js'
import { checkNpm } from '../check-npm.js'
import { packageJson } from '../package-json.js'
import { checkNode } from '../check-node.js'

export async function getProjectEnv() {
  const configs = getConfigs()
  // 期望的环境
  const wanted = getWanted(configs)
  console.log("🚀 . getProjectEnv . wanted:", wanted)
  // 当前运行的 实际环境
  const current = whichPmRuns()
  console.log("🚀 . getProjectEnv . current:", current)

  // // 配置 .gitignore 文件
  // const lockFileSuccess = await gitignoreLockfile(wanted.name)

  // // 配置 .npmrc 文件
  // const npmrcSuccess = await npmrcSettings(configs)

  // // 处理 package.json 文件
  // const packagejsonSuccess = await packageJson(wanted.nodeVersion, wanted.name, wanted.version)

  // // 检查 node
  // const nodeSuccess = await checkNode(wanted.nodeVersion, current.nodeVersion, configs.nodeMirror)

  // // 检查 包管理器版本
  // const pmSuccess = await checkNpm(wanted)

  // const doctors = {
  //   wanted,
  //   current,
  //   lockFile: lockFileSuccess,
  //   npmrc: npmrcSuccess,
  //   packageJson: packagejsonSuccess,
  //   node: nodeSuccess,
  //   packageManager: pmSuccess,
  // }
  // const hasError = Object.values(doctors).some((item) => item === false)
  // const errorMsg = hasError ? '项目环境切换失败' : '项目环境切换成功'
  // message[hasError ? 'error' : 'success'](`${errorMsg}:${JSON.stringify(doctors, null, 2)}`)
  // if (hasError) {
  //   process.exit(1)
  // }
}

export async function setProjectEnv() {
  const configs = getConfigs()
  message.log('🚀 . main . configs:', configs)
  // 期望的环境
  const wanted = getWanted(configs)
  // 当前运行的 实际环境
  const current = whichPmRuns()

  // 配置 .gitignore 文件
  const lockFileSuccess = await gitignoreLockfile(wanted.name)

  // 配置 .npmrc 文件
  const npmrcSuccess = await npmrcSettings(configs)

  // 处理 package.json 文件
  const packagejsonSuccess = await packageJson(wanted.nodeVersion, wanted.name, wanted.version)

  // 检查 node
  const nodeSuccess = await checkNode(wanted.nodeVersion, current.nodeVersion, configs.nodeMirror)

  // 检查 包管理器版本
  const pmSuccess = await checkNpm(wanted)

  const doctors = {
    wanted,
    current,
    lockFile: lockFileSuccess,
    npmrc: npmrcSuccess,
    packageJson: packagejsonSuccess,
    node: nodeSuccess,
    packageManager: pmSuccess,
  }
  const hasError = Object.values(doctors).some((item) => item === false)
  const errorMsg = hasError ? '项目环境切换失败' : '项目环境切换成功'
  message[hasError ? 'error' : 'success'](`${errorMsg}:${JSON.stringify(doctors, null, 2)}`)
  if (hasError) {
    process.exit(1)
  }
}
