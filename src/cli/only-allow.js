import { PackageManagers, message, whichPmRuns } from '../utils.js'

/**
 * 检测使用的包管理器和 engines 字段中要求的 是否一致
 * 不一致报错
 */
export function onlyAllow() {
  // const wantedPM = npm_package_engines_pnpm
  const [wantedPM] = Object.keys(PackageManagers).filter((name) => {
    return process.env[`npm_package_engines_${name}`]
  })
  const current = whichPmRuns()

  if (wantedPM && wantedPM != current.name) {
    console.log(box(`Please use "${wantedPM}"!`))
    process.exit(1)
  }
}

/**
 * https://www.npmjs.com/package/only-allow?activeTab=code
 */
function box(s) {
  const lines = s.trim().split('\n')
  const width = lines.reduce((a, b) => Math.max(a, b.length), 0)
  const surround = (x) => '║   \x1b[0m' + x.padEnd(width) + '\x1b[31m   ║'
  const bar = '═'.repeat(width)
  const top = '\x1b[31m╔═══' + bar + '═══╗'
  const pad = surround('')
  const bottom = '╚═══' + bar + '═══╝\x1b[0m'
  return [top, pad, ...lines.map(surround), pad, bottom].join('\n')
}
