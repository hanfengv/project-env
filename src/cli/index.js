#!/usr/bin/env node

/**
 * cli.js 文件开头必须是 #!/usr/bin/env node
 * 代表用 node 执行 cli.js
 */
import { onlyAllow } from './only-allow.js'
import { setDev } from './set-dev.js'
import { setProjectEnv, getProjectEnv } from './set-env.js'
import { randomPort } from '../utils.js'

function main() {
  // 配置项
  const args = process.argv
  // console.log('🚀 . main . args:', args)
  if (args.includes('dev')) {
    setDev()
  } else if (args.includes('onlyAllow')) {
    onlyAllow()
  } else if (args.includes('port')) {
    randomPort()
  } else if (args.includes('init')) {
    setProjectEnv()
  } else {
    getProjectEnv()
  }
}

main()
