import { switchNode } from '../check-node.js'
import { formatVersion, execAsync, message } from '../utils.js'

/**
 * 切换 node 和 包管理版本
 * 1. 此处需要检查node版本是否符合要求,不符合使用 n 切换node版本
 * 2. 开启包管理 version 需要 (corepack enable && corepack enable npm)
 */
export async function setDev() {
  // 当前的node版本
  const currentNode = formatVersion(process.version)
  const enginesNode = process.env.npm_package_engines_node
  message.log('🚀 . setDev :', { currentNode, enginesNode })

  if (currentNode !== enginesNode) {
    const nodeSuccess = await switchNode(enginesNode)
    const pmSuccess = await enableCorepack()
    if (!nodeSuccess) {
      message.error('node 版本切换失败')
      process.exit(1)
    }
  }
}

async function enableCorepack() {
  const command = `corepack enable`
  const { stdout, stderr } = await execAsync(command)
  return !stderr
}
