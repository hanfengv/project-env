import { readFile, writeFile } from 'node:fs/promises'
import { message } from './utils.js'

/**
 * 处理 npmrc文件
 * registry=https://registry.npmmirror.com/
 * engine-strict=true
 */
export async function npmrcSettings(configs = {}) {
  let success = true
  const { npmMirror } = configs
  try {
    const filePath = './.npmrc'
    const editKeys = ['registry=', 'engine-strict=']
    const editData = [`registry=${npmMirror}`, 'engine-strict=true', '']
    const Regx = new RegExp(editKeys.join('|'))

    const data = await readFile(filePath, { encoding: 'utf8', flag: 'a+' })

    const lines = data
      .trim()
      .split('\n')
      .filter((line) => !Regx.test(line))
    lines.push(...editData)
    const content = lines.join('\n')

    writeFile(filePath, content, { flag: 'w' })
  } catch (err) {
    success = false
    message.error('npmrc 错误', err)
  }
  return success
}
